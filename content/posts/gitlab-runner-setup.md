+++
title = 'Adventures in self hosting a dockerized GitLab runner'
date = 2024-05-14T13:56:36-06:00
draft = true
+++

I've been struggling to setup a self-hosted gitlab-runner for a while. I first started trying to set this up when I started my LibGDX Tower Defense. When I got to the point where I felt like the game was prototyped well enough, I wanted to make a release to show off the progress I had made. Some research showed that the best way to make a release with the binaries available is to use a runner.

I had a GitLab runner setup a long time ago, but one day I decided to reinstall the OS of that server and delete everything. At the time, I thought it would be pretty easy to set it up again. It turns out I was wrong. That's why I created this blog post. I am having a hard time finding resources and tutorials to explain how to get it set up the way I am trying to. This blog post is intended to help the future-me who tries to setup this runner again and runs into the same struggles, but hopefully other people might stumble on this post and find it helpful.

I want to dockerize this server as much as possible. Both because I don't want to deal with conflicting dependencies as I use this server for various things and to expand my knowledge of docker. I was hoping to run this GitLab runner completely in docker containers, but I'm starting to think that might be the root of the problems I am facing. I got to the point where I have a docker container setup, it registers itself to GitLab as a runner, but when I try to run any pipelines, it just refuses. My understanding is that GitLab runners work by starting up a docker container with the build environment, how can it do that if it itself is a docker container?

My next attempt will be to try running the GitLab runner on baremetal. 