+++
title = 'Godot makes Multiplayer incredibly easy'
date = 2024-06-07T12:38:56-06:00
draft = true
+++

Don't get me wrong here, adding multiplayer to my tower defense game didn't come without a few hours of pulling my hair out. It wasn't the walk in the park the title of this post suggests, and I'll explain a little about the issues I had soon. Despite that, though, it was worlds easier than I expected it to be. Overall it was a lot easier than multiplayer was on some other projects I've worked on. Syncing the world state between multiple players of a VRChat world, for example, was a nightmare in comparison.

Godot supports multiplayer natively and comes with nodes designed specifically to make it as easy as possible. This means you don't have to work out every aspect of the networking logic yourself. You add a few lines to your scripts to connect your games together, slap a couple nodes on the things you need to make sure get synced between players, and you're done. You have multiplayer support.

## How I got started
My journey started with this [YouTube Video by FinePointCGI](https://youtu.be/e0JLO_5UgQo?si=w_Vrm0U2ERGgSPZj). It's clear, concise, and it was incredibly helpful in getting me started.

The procedure is as follows:

1. Make a menu to for the player to choose whether they want to host the game or join an already hosted game.
2. Call Godot's [ENetMultiplayerPeer](https://docs.godotengine.org/en/stable/classes/class_enetmultiplayerpeer.html) to setup a client or server depending on which the player chose.
3. Hook into ENetMultiplayerPeer's signals so we can keep track of when players are connecting and disconnecting from eachother.
4. Setup [RPCs](https://docs.godotengine.org/en/stable/tutorials/networking/high_level_multiplayer.html#remote-procedure-calls) and an [Autoload](https://docs.godotengine.org/en/stable/tutorials/scripting/singletons_autoload.html) to track how many players are currently in the game.
5. Spawn player characters for each player in the game.
6. Add [MultiplayerSynchronizers](https://docs.godotengine.org/en/stable/classes/class_multiplayersynchronizer.html) to each player character and configure their multiplayer authority so each player can only control one.

## The issues
I ran into a few issues while following the video. Most of them were simply caused by wanting to things differently than they were in the video. This is where a friend of mine named [TeamLDM](https://www.youtube.com/@teamldm) was incredibly helpful. He's done more than enough suffering to setup multiplayer in his "bodybuilding co-op survival horror game" and was able to offer a ton of advice.

### My transition from menu to game worked differently
In the video, FinePointCGI starts the game when one of the player's hits the "Start Game" button. It does so by hiding the main menu, and spawning the game scene. Doing it this way makes sure that everyone has time to connect and update before the game scene loads. I wanted to do things a little differently. My game starts as soon as the the client connects to the server, and does so by deleting the menu scene from the tree and loading the game scene.

This caused a few issues for me. Because the RPC call to add each player to the autoload is in the menu scene and we would delete the menu scene before all the RPC calls would finish, my games wouldn't know that there was more then one player. The client game wouldn't even know about itself because it requires an RPC call from the server to add it to the list. This had a fairly easy solution. Moving the RPC function from the scene into the autoload script made it static so it always exists and the RPC calls would always go through.

### Why spawn characters manually when Godot can do it automatically?
Something that seemed very strange about FinePointCGI's video is the way the player characters were spawned. FinePointCGI wrote a script to spawn them. I had a hard time getting this working. Mostly due to the issue described above, but in troubleshooting that issue I found a really simple solution. Instead of having every single game independently spawn each player character, just have the server spawn them all, and use a [MultiplayerSpawner](https://docs.godotengine.org/en/stable/classes/class_multiplayerspawner.html) to sync them accross all clients.

It feels a little silly to have each client do this independently. Why do that when you can just have one of them do the work, and have Godot automatically sync it for you.

### Sometimes the MultiplayerSynchronizer can take over while you're in the middle of spawning a player character
The last and most frustrating problem I ran accross came from the MultiplayerSynchronizer taking over a little too soon. Maybe the way FinePointCGI had each client spawning all the player characters was a solution to this issue.

You see, when you instantiate something in Godot, it spawns it at the coordinate (0,0,0). In my game, there's no floor at (0,0,0), so anything spawned there would quickly fall to the void never to be seen again. I knew this, and I write my script so that immediately after spawning the player character, it would move it to a spawn point. Then in the player character's `_ready` function, it informs the MultiplayerSynchronizer who the multiplayer authority is. That is to say, "is this character controlled by this client? Or is it controlled by a different client?"

If you are not the multiplayer authority of an object, and the MultiplayerSynchronizer is syncing the position of the object, you cannot move it. So when the server spawns the player character, the multiplayer authority immediately gets set, and when it tries to reposition it in the very next line, nothing happens because it doesn't have the authority to do so anymore.

The solution was pretty simple. Make an RPC to tell the authority to move the player itself. The server spawns in the player character, the MultiplayerSynchronizer syncs that player character accross all the clients, then the RPC gets called telling the authority to move the player to the spawn point.

## Conclusion

This post is in no way trying to criticize FinePointCGI or his video. If you're here because you want to add multiplayer to your game, I strongly recommend watching his video. If you run into issues, come back to this blog post. Hopefully the answer to that problem will be here. Good luck! Game dev is hard and I hope I was able to make it a little easier.
