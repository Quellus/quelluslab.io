+++
title = 'Why Godot will become the industry standard game engine'
date = 2024-05-14T19:04:19-06:00
draft = true
+++

Blender has a history of having an absolutely horrible GUI. I remember trying to learn 3D modelling in high school and feeling overwhelmed. A short time later, I heard that Blender got a big GUI overhaul and that I should try again. Now, Blender is nearly industry standard.

When the largest game engines like Unity implement business practices that feel predatory like charging companies every time a customer reinstalls their game, open source alternatives like Godot look very attractive. I think Godot will follow the same path as Blender. Godot is still very young. In a post on the official Godot blog, it recognizes that Godot isn’t very performant when it comes to 3D rendering. It has a lot of growing to do.

Godot has a pretty big leg up on Blender. Its GUI is simple and clean, and its scripting language, GDScript, is familiar but straightforward. Godot is designed well enough from the beginning that someone new to programming and game dev wouldn't struggle too much to learn, and it's familiar enough that someone who knows their way around Unity would find it easy to get started in.

Godot has all the basic features it needs and is continuing to be developed. I don't think Godot will replace Unity and Unreal tomorrow, or even in a month, but it's well on its way. It needs some performance improvements, but it will get there.